require('dotenv').config()
var path = require("path");
var webpack = require("webpack");
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CompressionPlugin = require("compression-webpack-plugin");
var OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var autoprefixer = require('autoprefixer');
var ManifestPlugin = require('webpack-manifest-plugin');

process.traceDeprecation = true;
process.noDeprecation = true;

module.exports = {
  entry: [
    'whatwg-fetch',
    'babel-polyfill',
    path.join(__dirname, '../../src/app/index.js'),
    path.join(__dirname, '../../src/assets/images/favicon.png'),
  ],
  output: {
    path: path.join(__dirname, '..', '..', 'public', process.env.CURRENT_THEME),
    filename: 'assets/js/bundle.[hash].js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin([(`public/${process.env.CURRENT_THEME}`)], {
      root: path.join(__dirname, '..', '..'),
      verbose: true,
      dry: false,
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: 'src/html/index.html',
    }),
    new MiniCssExtractPlugin({
      filename: "assets/style/style.[hash].css",
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
        CURRENT_THEME: JSON.stringify(process.env.CURRENT_THEME),
      }
    }),
    new CompressionPlugin({
      asset: "[path].gz?[query]",
      algorithm: "gzip",
      test: /\.js|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new ManifestPlugin({
      fileName: `manifest.json`,
      generate: function (seed, files) {
        return files.reduce(function (manifest, {name, path, chunk}) {
          if (name.match('main\.js|main\.css')) {
            path = path.replace(/^.*[\\\/]/, '')
            return Object.assign({}, manifest, {[name]: path, [`${name}.gz`]: `${path}.gz`})
          } else {
            return Object.assign({}, manifest)
          }
        }, seed)
      }
    }),
  ],
  optimization: {
    runtimeChunk: false,
    minimizer: [
      new UglifyJsPlugin({
        parallel: true,
        uglifyOptions: {
          compress: {
            drop_console: true,
          },
          output: {
            comments: false,
            beautify: false,
          },
        }
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        exclude: [/node_modules/],
        use: ['babel-loader']
      },
      {
        test: /\.scss/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {modules: true, importLoaders: 1, localIdentName: '[sha256:hash:base64:8]'},
          },
          "postcss-loader",
          'resolve-url-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "postcss-loader"
        ]
      },
      {
        test: /\.(png|jpg|gif|ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: {
          loader: "file-loader",
          query: {
            name: '/assets/files/[name].[ext]?[hash]',
            limit: '10000'
          }
        }
      },
    ],
  }
};
