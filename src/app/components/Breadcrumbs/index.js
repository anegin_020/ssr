import React from 'react'
import PropTypes from 'prop-types'
import Immutable from 'immutable'
import ControlLink from '../ControlLink'
import styles from './styles.scss'

export default class Breadcrumbs extends React.PureComponent {

  renderBreadcrumb({text, link}, key, active) {
    return <li key={key}>
      {active === true
        ? <ControlLink to={link}>{text}</ControlLink>
        : <span>{text}</span>}
    </li>
  }

  render() {
    const {data} = this.props
    const length = data.count() - 1
    return <section className={styles.body}>
      <ul>
        {data.map((_data, key) => this.renderBreadcrumb(_data, key, length !== key))}
      </ul>
    </section>
  }

}

Breadcrumbs.propTypes = {
  data: PropTypes.instanceOf(Immutable.List)
}
