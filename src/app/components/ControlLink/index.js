import React from 'react'
import {Link} from 'react-router-dom'

export default class ControlLink extends React.PureComponent {

  render(){
    return <Link {...this.props}/>
  }

}
