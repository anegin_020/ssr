import React from 'react';
import PropTypes from 'prop-types';
const styles = require(`./styles.${process.env.NODE_CURRENT_THEME}.scss`)

export default class Label extends React.PureComponent {

    static propTypes = {
        context: PropTypes.string,
        htmlFor: PropTypes.string.isRequired,
        bold: PropTypes.bool,
        className: PropTypes.string,
    };

    static defaultProps = {

    };

    render() {
        const {context, htmlFor = '', children, bold, className} = this.props;
        return (
            <label
                className={`${styles.Label}${!bold ? '' : ` ${styles.bold}`}${context !== undefined ? ` ${styles[context]}` : ''}${className !== undefined ? ` ${className}` : ''}`}
                htmlFor={htmlFor}
            >
                {children}
            </label>
        );
    }

}