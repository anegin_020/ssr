import React from 'react'
import TemplateComponent from '../../../TemplateComponent'
import Label from '../LabelLayout/Label';

class DevelopmentLabelLayout extends React.PureComponent {

  render (){
    return (
      <div>
        <Label
          htmlFor="body"
          bold
        >
          Test Label
        </Label>
      </div>
    )
  }

}

export default TemplateComponent(DevelopmentLabelLayout)
