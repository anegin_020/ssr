import React from 'react'
import PropTypes from 'prop-types'
import {hot} from 'react-hot-loader'
//import Immutable from 'immutable'
import styles from './test1.scss'

const APPLE_SIZE = [
  'Little',
  'Medium',
  'Big'
]

function getRandomAppleSize() {
  return APPLE_SIZE[Math.floor(Math.random() * 3)]
}

const SortBasket = ({apples}) => (
  <div>
    {APPLE_SIZE.map((size, key) => <p key={key}>{size}: {(apples.filter(apple => apple.size === size)).length}</p>)}
  </div>
)

const Basket = ({apples}) => (
  <section className={styles.basketInfo}>
    <SortBasket apples={apples}/>
  </section>
)

class BasketController extends React.PureComponent {

  render() {
    return <div className={styles.basket}>
      <div className={styles.basketController}>
        <button
          className={styles.basketBtn}
          onClick={this.props.onClickInc}
        >
          +
        </button>
        <button
          className={styles.basketBtn}
          onClick={this.props.onClickDec}
        >
          -
        </button>
      </div>
      <Basket apples={this.props.apples}/>
    </div>
  }

}
Basket.propTypes = {
  apples: PropTypes.array,
  onClickInc: PropTypes.func,
  onClickDec: PropTypes.func,
}

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      apples: [],
      sunnyWeather: true,
    }
  }

  render() {
    const {apples, sunnyWeather} = this.state
    return <div className={`${styles.body} ${sunnyWeather === true ? styles.sunny : styles.lunar}`}>
      <h1 className={styles.title}>Basket of apples</h1>
      <button
        className={styles.weather}
        onClick={() => {this.setState({sunnyWeather: !this.state.sunnyWeather})}}
      >
        ~
      </button>
      <BasketController
        onClickInc={() => {
          let _apples = this.state.apples
          _apples.push({size: getRandomAppleSize()})
          this.setState({apples: _apples})
        }}
        onClickDec={() => {
          let _apples = this.state.apples
          let _removeKey = Math.floor(Math.random() * _apples.length)
          _apples = _apples.filter((apple, key) => key !== _removeKey)
          this.setState({apples: _apples})
        }}
        apples={apples}
      />
    </div>
  }

}

export default hot(module)(App)

