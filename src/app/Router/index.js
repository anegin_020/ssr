if (process.env.NODE_ENV === 'production') {
  module.exports = require('./Router')
} else {
  module.exports = require('./Router.development')
}
