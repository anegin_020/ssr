import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import App from '../containers/App'
import Users from '../containers/Users'
import User from '../containers/User'
import Home from '../containers/Home'

import permissionWrapper from '../utils/permissions/permissionWrapper'

export const Routes = [
  {
    path: "/",
    component: Home,
    exact: true,
  }, {
    path: "/users/:id",
    component: permissionWrapper(User, 'authPermission'),
  }, {
    path: "/users",
    component: Users,
    exact: true,
  }, {
    path: "",
    component: () => (<h1>Not found</h1>)
  }
];

export default () => (
  <App>
    <Switch>
      {Routes.map((route, i) => <Route {...route} key={i}/>)}
    </Switch>
  </App>
)
