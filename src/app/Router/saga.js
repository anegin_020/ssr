import {all, takeEvery, takeLatest, put, call, select, fork, cancel} from 'redux-saga/effects'
import {LOCATION_CHANGE} from 'react-router-redux'
import matchPath from 'react-router-dom/matchPath'
import {Routes} from '../Router/Router'
import App from '../containers/App'

function* loadData({payload: {location}}) {
  for(let key in Routes){
    const match = matchPath(location.pathname, {path: Routes[key].path, exact: Routes[key].exact})
    if(match !== null){
      //console.log("%c"+'CHECK MATCH', 'color: green', Routes[key].component.breadcrumbs)
      if(Routes[key].component.checkPermission !== undefined){
        yield Routes[key].component.checkPermission()
      }
      if(Routes[key].component.loadData !== undefined){
        yield put(Routes[key].component.loadData(match.params, location.search))
      }
      break;
    }
  }
  if(App.checkLoadData !== undefined){
    const load = yield select(App.checkLoadData())
    if(load !== true){
      yield put(App.loadData())
    }
  }
}

export default function*() {
  yield all([
    yield takeLatest(LOCATION_CHANGE, loadData),
  ])
}
