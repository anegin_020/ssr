import React from 'react'
import ComponentsNavigation from './main/ComponentsNavigation'

export default function (WrappedComponent) {
  return class extends React.PureComponent {
    render() {
      return (
        <section>
          <header>
            <ComponentsNavigation/>
          </header>
          <div>
            <WrappedComponent {...this.props} />
          </div>
        </section>
      )
    }
  }
}
