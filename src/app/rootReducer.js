import {combineReducers} from 'redux'
import { routerReducer } from 'react-router-redux'
import App from './containers/App/reducer'
import Users from './containers/Users/reducer'
import User from './containers/User/reducer'
import Modals from './containers/Modals/reducer'
import Home from './containers/Home/reducer'

export default combineReducers({
  router: routerReducer,
  App,
  Home,
  User,
  Users,
  Modals
})
