import moduleA from './test/moduleA'
import moduleB from './test/moduleB'

export default {
  moduleA,
  moduleB
}
