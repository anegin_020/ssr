import Immutable from 'immutable'
import Serialize from 'remotedev-serialize'
import * as UsersRecords from '../containers/Users/record'
import * as AppRecords from '../containers/App/record'
import * as HomeRecords from '../containers/Home/record'
import * as UserRecords from '../containers/User/record'

const { stringify, parse } =  Serialize.immutable(Immutable, [
  UsersRecords.UserRecord,
  AppRecords.UserRecord,
  AppRecords.BreadcrumbRecord,
  HomeRecords.PostRecord,
  UserRecords._CompanyRecord,
  UserRecords._GeoRecord,
  UserRecords._AddressRecord,
  UserRecords._UserRecord,
]);

export const serialize = (state) => {
  return JSON.stringify(stringify(state));
}

export const deserialize = (serialized = JSON.stringify({})) => {
  return parse(serialized)
}
