import LocalStorage from '../storage/LocalStorage/index'

/**
 * @function get {Promise} выполняет GET запрос. url(arg1) {string} строка с url к которой применяется функция transformUrl; body(arg2) {object} тело запроса к которому применяется функция setParamsUrl в случае если options.withForm = false; withToken(arg3) {boolean} по умолчанию true, отключить в случае если инлайнить token в запрос нет необходимости; withForm(arg3) {boolean} по умолчанию false, включить если есть необъодимость отправить FORM; withRest(arg3) {boolean} по умолчанию true, отключить в случае если REST запрос не нужен
 * @function getWithStorage {Promise} выполняет GET запрос. url(arg1) {string} строка с url к которой применяется функция transformUrl; body(arg2) {object} тело запроса к которому применяется функция setParamsUrl в случае если options.withForm = false; withToken(arg3) {boolean} по умолчанию true, отключить в случае если инлайнить token в запрос нет необходимости; withForm(arg3) {boolean} по умолчанию false, включить если есть необъодимость отправить FORM; withRest(arg3) {boolean} по умолчанию true, отключить в случае если REST запрос не нужен
 * @function post {Promise} выполняет POST запрос. url(arg1) {string} строка с url к которой применяется функция transformUrl; body(arg2) тело запроса; withToken(arg3) {boolean} по умолчанию true, отключить в случае если инлайнить token в запрос нет необходимости; withForm(arg3) {boolean} по умолчанию false, включить если есть необъодимость отправить FORM; withRest(arg3) {boolean} по умолчанию true, отключить в случае если REST запрос не нужен
 * @function getConfig {object} Возвращает объект с настройками Api
 * @function setDomain {undefined} Устанавливает domain для Api
 * @function setToken {undefined} Устанавливает token для Api
 * @function removeToken {undefined} Удаляет token
 * @function removeDomain {undefined} Удаляет domain
 * @function isToken {boolean} Проверяет установлен ли domain для Api
 * @function isDomain {boolean} Проверяет установлен ли token для Api
 * @return {Api}
 */
export default (function () {

  let _instance = undefined
  let _domain = ''
  let _isDomain = false
  let _token = undefined
  let _isToken = false

  let _statusSuccess = (status) => (status >= 200 && status < 300)
  /**
   * Преобразовывает URL относительно текущего domain или оставляет исходный если начинается с [http,https]
   * @param url {string} входной URL
   * @return {string}
   * @private
   */
  let _transformUrl = (url) => {
    if (!url.match(/^https?/)) {
      url = url[0] !== '/' ? `/${url}` : url
      url = `${_domain}${url}`
    }
    return url
  }
  /**
   * Выполняет fetch запрос
   * @param method {string} [GET, POST] Метод запроса
   * @param url {string} URL на который будет выполнен запрос
   * @param body {object} Тело запроса. В случае если запрос является GET-ом то к нему применяется функция Api.setParamsUrl
   * @param withToken {boolean} Подстовлять ли в тело запроса token
   * @param withForm {boolean} В случае если флаг выставлен в true, то запрос будет эмитировать отправку FORM
   * @param withRest {boolean} Устанавливает credentials заголовки для выполнения REST запросов
   * @return {Promise}
   * @private
   */
  let _request = (method, url, body, withToken, {withForm = false, withRest = true}) => {
    let request = {
      method: method,
      credentials: withRest === true ? 'same-origin' : 'include',
    }
    if(withToken === true){
      body = body !== undefined ? {token: _token, ...body} : {token: _token}
    }
    if (body !== undefined) {
      if(method === 'GET'){
        url = setParamsUrl(url, body)
      } else {
        request.body = withForm !== true
          ? JSON.stringify(body)
          : body
      }
    }
    if (withForm !== true) {
      request.headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }
    console.log('_request', url)
    return fetch(_transformUrl(url), request)
      .then(response => ((response.status >= 500 || response.status === 404)
        ? {response}
        : response.json().then(json => ({ json, response })))
      ).then(({ json, response }) => {
        if (_statusSuccess(response.status)) {
          if(process.env.NODE_FIX_WIN_PATH === '1'){
            json = JSON.parse(JSON.stringify(json).replace(/\\([A-Za-z0-9])/g, '/$1').replace(/\/Mod/g, '\\Mod'))
          }
          if(_domain !== ''){
            const regExp = new RegExp(`${_domain}\/?`, 'g')
            json = JSON.parse(JSON.stringify(json).replace(regExp, '/'))
          }
          return json
        }
        if (response.status === 404) {
          return Promise.reject({
            errors: {servers: `Response with status ${response.status}.`},
            status: response.status,
          });
        }
        if (response.status >= 500) {
          return Promise.reject({
            errors: {servers: `Response with Server error (${response.status}).`},
            status: response.status,
          });
        }
        if (response.status >= 300 && response.status < 400) {
          return Promise.reject({
            errors: {servers: `Response with redirect ${response.status}`},
            status: response.status,
          });
        }
        return Promise.reject(json)
      })
  }

  class Api {

    constructor() {
      if (_instance === undefined) {
        _instance = this
      }
      return _instance
    }

    setDomain = (domain = '') => {
      _domain = domain
      _isDomain = true
    }
    setToken = (token) => {
      _token = token
      _isToken = true
    }
    removeToken = () => {
      _token = undefined
      _isToken = false
    }
    removeDomain = () => {
      _domain = ''
      _isDomain = false
    }
    isDomain = () => _isDomain
    isToken = () => _isToken

    getConfig = () => ({
      domain: _domain,
      token: _token,
    })

    get = (url, body, {...options}) => _request('GET', url, body, false, options)
    getAuth = (url, body, {...options}) => _request('GET', url, body, true, options)
    post = (url, body, {...options}) => _request('POST', url, body, false, options)
    postAuth = (url, body, {...options}) => _request('POST', url, body, true, options)
    joinStorage = (request, url, body, {...options}) => {
      let data = LocalStorage.get(url);
      if (data) {
        return Promise.resolve(data);
      }
      return request(url, body, options)
        .then(json => LocalStorage.set(url, json, options.timestamp))
    }
  }

  let instance = new Api()
  return instance

})()

/**
 * Преобразует входной объект в строку с GET параметрами [?token=xxx&domain=test]
 * @param url {string}
 * @param params {object}
 * @return {string}
 */
export function setParamsUrl(url, params = {}) {
  let parse = url.match(/^([^\?]*)|\?([^&]*)|&([^&]*)/gi)
  let count = parse.length - 1
  for (let key in params) {
    if (params[key] !== undefined) {
      if (Array.isArray(params[key]) === true) {
        params[key] = JSON.stringify(params[key])
      }
      if (!count) {
        url += `?${key}=${params[key]}`
      } else {
        url += `&${key}=${params[key]}`
      }
      ++count
    }
  }
  return url
}
