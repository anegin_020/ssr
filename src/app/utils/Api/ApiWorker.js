import {select, call} from 'redux-saga/effects'
import {makeDomain} from '../../containers/App/selector'
import Api from './index'

export default function* (...args) {
  if(Api.isDomain() !== true){
    let domain = yield select(makeDomain)
    if(domain === undefined){
      domain = window !== undefined ? window.location.hostname : undefined
    }
    Api.setDomain(domain)
  }
  return yield call(args[0], Api, args.slice(1,args.length))
}
