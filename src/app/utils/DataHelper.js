export function increseSeconds(n){
  var d = new Date();
  return d.setSeconds(d.getSeconds() + n);
}

export function increseMinutes(n){
  var d = new Date();
  return d.setMinutes(d.getMinutes() + n);
}

export function increseHours(n){
  var d = new Date();
  return d.setHours(d.getHours() + n);
}

export function increseDate(n){
  var d = new Date();
  return d.setDate(d.getDate() + n);
}

