import {all, takeEvery, takeLatest, put, call, select, fork, cancel} from 'redux-saga/effects'
import {replace} from 'react-router-redux'
import {makeControlLocationPrev, makeUser} from '../../containers/App/selector'

export function* authPermission() {
  const user = yield select(makeUser)
  if (user.get('auth') !== true) {
    yield put(replace('/users'))
    yield cancel()
  }
}

export default {
  authPermission
}
