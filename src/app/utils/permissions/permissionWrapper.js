if (process.env.NODE_APP === 'server') {
  module.exports = function (Component, authKey){
    Component.checkPermission = (require('./_serverPermissions').default)[authKey]
    return Component
  }
} else {
  module.exports = function (Component, authKey){
    const permissions = require('./_clientPermissions').default
    if(permissions[authKey] !== undefined){
      return permissions[authKey](Component)
    }
    return Component
  }
}
