import React from 'react'
import PropTypes from 'prop-types'
import {
  Redirect,
} from "react-router-dom";

function authPermission (Component) {

  class PermissionTrashRoute extends React.Component {

    state = {
      isAuth: undefined,
    }

    static contextTypes = {
      store: PropTypes.object,
      router: PropTypes.object
    }

    componentDidMount(){
      const { store } = this.context
      if(store.getState().App.get('user').get('auth') !== true){
        this.setState({isAuth: false})
      }
    }

    render () {
      switch (this.state.isAuth){
        case false:
          return <Redirect
            to={{
              pathname: "/users",
              state: { from: this.props.location }
            }}
          />
        case true:
          return React.createElement(
            Component,
            { ...this.props }
          )
        default:
          return null
      }
    }

  }

  return PermissionTrashRoute

}

export default {
  authPermission
}
