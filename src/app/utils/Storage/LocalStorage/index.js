if(typeof localStorage === "undefined" || localStorage === null){
  module.exports = require('./ServerLocalStorage')
} else {
  module.exports = require('./ClientLocalStorage')
}
