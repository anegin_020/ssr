import React from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import ControlLink from '../../components/ControlLink'
import ContainerInterface from '../helpers/ContainerInterface'
import * as HomeActions from './action'
import * as HomeSelectors from './selector'
import styles from './styles.scss'


@connect((state) => createStructuredSelector({
  load: HomeSelectors.makeLoad,
  top10Posts: HomeSelectors.makeTop10Posts,
}))
export default class Home extends ContainerInterface{

  static loadData = () => HomeActions.loadData()
  static checkLoadData = () => HomeSelectors.makeLoad

  static breadcrumbs = () => ([
    {
      text: 'Главная',
      link: '/'
    }
  ])

  componentDidMount() {
    if(this.props.load === false){
      this.props.dispatch(Home.loadData())
    }
  }

  render(){
    const {top10Posts} = this.props
    return <section>
      <h1>Главная страница</h1>
      <ControlLink to="/users">Пользователи</ControlLink>
      <br/>
      <h2>Top 10 posts:</h2>
      {top10Posts.map((post, key) => <div className={styles.post} key={key}>
        <h3>{post.title}</h3>
        <div dangerouslySetInnerHTML={{__html: post.body}}/>
      </div>)}
    </section>
  }

}
