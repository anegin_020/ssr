import {createSelector} from 'reselect'

const selectHome = (state) => state.Home

const makeLoad = createSelector(
  selectHome,
  (state) => state.get('load')
)

const makeTop10Posts = createSelector(
  selectHome,
  (state) => state.get('top10Posts')
)

export {
  makeLoad,
  makeTop10Posts,
}
