import * as HomeRecords from './record'

export const getPostsService = (api) => {
  return api.get('https://jsonplaceholder.typicode.com/posts')
    .then(response => ({response: response.splice(0, 10).map(post => new HomeRecords.PostRecord({...post}))}))
    .catch(errors => errors)
}
