import * as HomeConstants from './constant'

export const loadData = () => ({
  type: HomeConstants.DATA.REQUEST,
})
