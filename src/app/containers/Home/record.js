import Immutable from 'immutable'

export const PostRecord = Immutable.Record({
  id: undefined,
  userId: undefined,
  title: undefined,
  body: undefined,
})
