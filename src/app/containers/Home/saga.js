import {all, put, takeLatest} from 'redux-saga/effects'
import * as HomeConstants from './constant'
import {setBreadcrumbs} from '../App/action'
import Home from './'
import ApiWorker from '../../utils/Api/ApiWorker'
import {getPostsService} from './service'

function* loadData(){
  const {response, errors} = yield ApiWorker(getPostsService)
  if(response){
    yield put({type: HomeConstants.DATA.SUCCESS, payload: {posts: response}})
    yield put(setBreadcrumbs(Home.breadcrumbs()))
  } else {

  }
}

export default function*() {
  yield all([
    yield takeLatest(HomeConstants.DATA.REQUEST, loadData)
  ])
}
