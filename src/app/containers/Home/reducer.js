import Immutable from 'immutable'
import * as HomeConstants from './constant'

const initialState = Immutable.Map({
  load: false,
  top10Posts: Immutable.List(),
})

export default (state = initialState, action) => {
  switch (action.type) {
    case HomeConstants.DATA.SUCCESS:
      return state
        .set('load', true)
        .set('top10Posts', Immutable.List(action.payload.posts))
    default:
      return state
  }
}
