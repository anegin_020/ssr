import defineConstant from '../helpers/defineConstant'

export const DATA = defineConstant('Home', 'data', ['SUCCESS', 'REQUEST', 'FAILURE'])
