import React from 'react'
import ReactDom from 'react-dom'
import PropTypes from 'prop-types'
import {canUseDOM} from 'exenv'
import styles from './styles.scss'

export default class UserDetails extends React.PureComponent {

  modal = undefined
  wrapper = undefined

  componentDidMount() {
    if (canUseDOM) {
      this.modal = document.getElementById('appModals')
      this.wrapper = document.createElement('div')
      this.modal.appendChild(this.wrapper);
      this.forceUpdate()
    }
  }

  componentWillUnmount() {
    if (canUseDOM) {
      this.modal.removeChild(this.wrapper);
    }
  }

  renderUser = () => {
    const {user} = this.props
    return <div className={styles.content}>
      <p>ID: {user.id}</p>
    </div>
  }

  render() {
    return this.wrapper !== undefined
      ? ReactDom.createPortal(
        <div className={styles.body}>
          {this.renderUser()}
        </div>,
        this.wrapper
      ) : null
  }

}

UserDetails.propTypes = {
  user: PropTypes.any,
}
