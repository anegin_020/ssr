import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.scss'

export default class User extends React.PureComponent {

  handleClick = () => {
    if(this.props.user.get('id') !== undefined){
      this.props.onShowDetails(this.props.user.get('id'))
    }
  }

  render(){
    const {user} = this.props
    return <tr className={styles.body} onClick={this.handleClick}>
      <td>{user.get('username')}</td>
      <td>{user.get('email')}</td>
      <td>{user.get('name')}</td>
      <td>{user.get('website')}</td>
      <td>{user.get('address').geo.lat}</td>
    </tr>
  }

}

User.propTypes = {
  onShowDetails: PropTypes.func.isRequired,
  user: PropTypes.shape({
    username: PropTypes.string,
    email: PropTypes.string,
    name: PropTypes.string,
    website: PropTypes.string,

  })
}
