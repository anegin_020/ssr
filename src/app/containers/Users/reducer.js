import Immutable from 'immutable'
import * as UsersConstants from './constant'

const initialState = Immutable.Map({
  load: false,
  users: Immutable.List(),
})

export default (state = initialState, action) => {
  switch (action.type) {
    case UsersConstants.Users_content_SUCCESS:
      return state.set('users', Immutable.List(action.payload))
        .set('load', true)
    default:
      return state
  }
}
