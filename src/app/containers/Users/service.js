import {increseSeconds} from '../../utils/DataHelper'
import {UserRecord} from './record'

export const getUsersService = (api) => {
  return api.get('https://jsonplaceholder.typicode.com/users')
    .then(response => ({response: response.map(user => new UserRecord({...user}))}))
    .catch(errors => errors)
}
