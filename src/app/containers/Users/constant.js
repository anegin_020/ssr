export const Users_content_REQUEST = 'Users_content_REQUEST'
export const Users_content_SUCCESS = 'Users_content_SUCCESS'
export const Users_content_FAILURE = 'Users_content_FAILURE'

export const Users_userDetails_REQUEST = 'Users_userDetails_REQUEST'
export const Users_userDetails_SUCCESS = 'Users_userDetails_SUCCESS'
export const Users_userDetails_FAILURE = 'Users_userDetails_FAILURE'
