import * as UsersConstants from './constant'

export const content = () => ({
  type: UsersConstants.Users_content_REQUEST,
})

export const userDetails = (userId) => ({
  type: UsersConstants.Users_userDetails_REQUEST,
  payload: userId,
})

