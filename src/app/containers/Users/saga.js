import {delay} from 'redux-saga'
import {all, takeEvery, put, call, select} from 'redux-saga/effects'
import * as UsersConstants from './constant'
import {getUsersService} from './service'
import Users from './'
import {setBreadcrumbs} from '../App/action'
import ApiWorker from '../../utils/Api/ApiWorker'

function* content() {
  const {response, errors} = yield ApiWorker(getUsersService)
  if(response){
    yield put({
      type: UsersConstants.Users_content_SUCCESS,
      payload: response,
    })
    yield put(setBreadcrumbs(Users.breadcrumbs()))
  }
}

export default function*() {
  yield all([
    yield takeEvery(UsersConstants.Users_content_REQUEST, content),
  ])
}
