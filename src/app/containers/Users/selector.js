import { createSelector } from 'reselect'

const selectHome = (state) => state.Users

const makeUsers = createSelector(
  selectHome,
  (state) => state.get('users')
)

const makeLoad = createSelector(
  selectHome,
  (state) => state.get('load')
)

export {
  selectHome,
  makeUsers,
  makeLoad,
}
