import Immutable from 'immutable'

export const UserRecord = Immutable.Record({
  id: undefined,
  name: undefined,
  username: undefined,
  email: undefined,
  website: undefined,
  address: {},
})
