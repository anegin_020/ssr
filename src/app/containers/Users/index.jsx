import React from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import {replace} from 'react-router-redux'
import * as UsersSelectors from './selector'
import * as UsersActions from './action'
import * as AppActions from '../App/action'
import {set as setModal} from '../Modals/action'
import User from './components/User'
import UserDetails from './components/UserDetails'
import ContainerInterface from '../helpers/ContainerInterface'
import styles from './styles.scss'

@connect((state) => createStructuredSelector({
  users: UsersSelectors.makeUsers,
  load: UsersSelectors.makeLoad,
}))
export default class Home extends ContainerInterface {

  static loadData = (params) => UsersActions.content()
  static checkLoadData = () => UsersSelectors.makeLoad
  static breadcrumbs = () => ([
    {
      text: 'Главная',
      link: '/'
    },{
      text: 'Пользователи',
      link: '/users'
    }
  ])

  componentDidMount() {
    if(this.props.load === false){
      this.props.dispatch(Home.loadData())
    }
  }

  handleShowDetails = (userId) => {
    this.props.dispatch(replace(`/users/${userId}`))
  }

  render() {
    const {users} = this.props
    return (
      <div className={styles.body}>
        <h1 className={styles.h2}>Page Users</h1>
        <table>
          <thead>
          <tr>
            <th>Никнейм</th>
            <th>E-mail</th>
            <th>Имя</th>
            <th>Вебсайт</th>
            <th>Город</th>
          </tr>
          </thead>
          <tbody>
          {users.map((user, key) =>
            <User
              key={key}
              user={user}
              onShowDetails={this.handleShowDetails}
            />
          )}
          </tbody>
        </table>
        <h2>Users({users.count()}):</h2>
      </div>
    )
  }

}
