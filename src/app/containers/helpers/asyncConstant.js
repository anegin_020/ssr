/**
 * Создание ассинхронной константы
 * @param prefix
 * @param action
 * @return {{SUCCESS: string, REQUEST: string, FAILURE: string}}
 */
export default (prefix, action) => ({
  SUCCESS: `${prefix}_${action}`,
  REQUEST: `${prefix}_${action}_REQUEST`,
  FAILURE: `${prefix}_${action}_FAILURE`,
})
