const DEFAULT_NAMESPACE = 'Main'
const DEFAULT_SEPARATOR = '/'

/**
 * Генерация констант событий
 * @param argv[0] {string} Префикс имени
 * @param argv[1] {string} Название события
 * @param argv[2] {array|string} В случае если аргумент является массивом будет сгенерирован объект с ключами, являющемися элементами массива, и значениями, использующими эти ключи для подстановки постфикса. В случае если аргумент является строкой, то данный аргуент будет использоваться как пространтсво имен, смотреть описание argv[3]
 * @param argv[3] {string} Пространсто имен для константы, подставлется в начало константы. По умолчанию берется значние DEFAULT_NAMESPACE и разделяется DEFAULT_SEPARATOR
 * @return {string|{}}
 * @example
 * defineConstant('App', 'incLike') // "Main/App_incLike"
 * defineConstant('App', 'incLike', 'Admin') // "Admin/App_incLike"
 * defineConstant('App', 'incLike', ['SUCCESS', 'REQUEST', 'FAILURE']) // {"SUCCESS":"Main/App_incLike_SUCCESS","REQUEST":"Main/App_incLike_REQUEST","FAILURE":"Main/App_incLike_FAILURE"}
 * defineConstant('App', 'incLike', ['SUCCESS', 'REQUEST', 'FAILURE'], 'Admin') // {"SUCCESS":"Admin/App_incLike_SUCCESS","REQUEST":"Admin/App_incLike_REQUEST","FAILURE":"Admin/App_incLike_FAILURE"}
 */
export default function(...argv) {

  let namespace = argv[3] !== undefined ? argv[3] : DEFAULT_NAMESPACE

  if (typeof argv[2] === 'string') {
    namespace = argv[2]
  }

  let constant = `${namespace}${DEFAULT_SEPARATOR}${argv[0]}_${argv[1]}`

  if (Array.isArray(argv[2])) {
    return argv[2].reduce((prev, next) => Object.assign({}, prev, {[next]: `${constant}_${next}`}), '')
  }

  return constant

}
