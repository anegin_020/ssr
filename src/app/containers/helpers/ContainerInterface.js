import React from 'react'

export default class ContainerInterface extends React.Component{

  /**
   * Вызывается в saga для загрузки данных необходимых компоненту. Прослушиваемое событие @@router/LOCATION_CHANGE
   * @param params колбэк функция принимает агрумент URL параметр, полученный после парсинга react-router/mathPath
   * @type {function|undefined} функция должна возвращать интерфейс Action {type: ''}
   * @example () => ({ type: 'REQUEST' })
   */
  static loadData = undefined

  /**
   * Выполняет redux-saga/effects/select для проверки необходима ли загрузка данных функцией loadData
   * @type {action|undefined} функция должна возвращать функцию реализующую интерфейс reselect/createSelector
   * @example () => (createSelector( selectData, (state) => state.get('data') ))
   */
  static checkLoadData = undefined

  /**
   * @type {function|undefined} функция должна возвращать array. К этой функции применяется App/action/setBreadcrumbs
   * @example () => ([{text: '', link: ''}])
   */
  static breadcrumbs = undefined

}
