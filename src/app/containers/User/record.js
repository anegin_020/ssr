import Immutable from 'immutable'

export const _CompanyRecord = Immutable.Record({
  name: undefined,
  catchPhrase: undefined,
  bs: undefined,
})

export const _GeoRecord = Immutable.Record({
  lat: undefined,
  lng: undefined,
})

export const _AddressRecord = Immutable.Record({
  street: undefined,
  suite: undefined,
  city: undefined,
  zipcode: undefined,
  geo: undefined,
})

export const _UserRecord = Immutable.Record({
  id: undefined,
  name: undefined,
  username: undefined,
  email: undefined,
  address: undefined,
  phone: undefined,
  website: undefined,
  company: undefined,
})

export const UserRecord = (data) => (
  data !== undefined
    ? new _UserRecord({
    ...data,
    address: data.address !== undefined
      ? new _AddressRecord({
        ...data.address,
        geo: data.address.geo !== undefined
          ? new _GeoRecord(data.address.geo)
          : undefined
      })
      : undefined,
    company: data.company !== undefined
      ? new _CompanyRecord(data.company)
      : undefined
  })
    : undefined
)
