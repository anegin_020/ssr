import Immutable from 'immutable'
import * as UserConstants from './constant'

const initialState = Immutable.Map({
  load: false,
  user: undefined,
})

export default (state = initialState, action) => {
  switch (action.type) {
    case UserConstants.data.SUCCESS:
      return state.set('user', action.payload)
        .set('load', true)
    case UserConstants.destroy:
      return initialState
    default:
      return state
  }
}
