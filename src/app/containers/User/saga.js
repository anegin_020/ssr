import {all, takeEvery, put, call, select} from 'redux-saga/effects'
import * as UserConstants from './constant'
import {loadUserService} from './service'
import {setBreadcrumbs} from '../App/action'
import User from './'
import ApiWorker from '../../utils/Api/ApiWorker'

function* content({payload}) {
  const {response, errors} = yield ApiWorker(loadUserService, payload)
  if(response){
    yield put({
      type: UserConstants.data.SUCCESS,
      payload: response,
    })
    yield put(setBreadcrumbs(User.breadcrumbs(response)))
  } else {
    console.log(errors)
  }
}

export default function*() {
  yield all([
    yield takeEvery(UserConstants.data.REQUEST, content),
  ])
}
