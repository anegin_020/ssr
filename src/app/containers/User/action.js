import * as UserConstants from './constant'

export const loadData = (id) => ({
  type: UserConstants.data.REQUEST,
  payload: id
})

export const destroy = () => ({
  type: UserConstants.destroy
})
