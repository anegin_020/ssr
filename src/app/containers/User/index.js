import React from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import * as UserActions from './action'
import * as UserSelectors from './selector'
import ContainerInterface from '../helpers/ContainerInterface'
import ControlLink from '../../components/ControlLink'
import styles from './styles.scss'

@connect((state) => createStructuredSelector({
  load: UserSelectors.makeLoad,
  user: UserSelectors.makeUser,
}))
export default class User extends ContainerInterface {

  static loadData = (params) => {
    return UserActions.loadData(params.id)
  }
  static checkLoadData = () => UserSelectors.makeLoad
  static breadcrumbs = (user) => {
    let _breadcrumbs = [
      {
        text: 'Главная',
        link: '/'
      },{
        text: 'Пользователи',
        link: '/users'
      }
    ]
    if(user !== undefined){
      _breadcrumbs = _breadcrumbs.concat({
        text: user.username,
        link: `/users/${user.id}`
      })
    }
    return _breadcrumbs
  }

  componentDidMount() {
    if(this.props.load === false){
      this.props.dispatch(User.loadData())
    }
  }

  render(){
    const {load, user} = this.props
    if(load === false) return null
    if(user !== undefined) {
      return <div className={styles.body}>
        <ControlLink to={`/users/${user.id + 1}`}>Next user</ControlLink>
        <br/>
        <ControlLink to={`/users/${user.id}?numb=${Math.random()}`}>Random route</ControlLink>
        <p><span className={styles.label}>Имя</span>{user.name}</p>
        <p><span className={styles.label}>Email</span>{user.email}</p>
        {user.address !== undefined &&
        <React.Fragment>
          <p><span className={styles.label}>Город</span>{user.address.city}</p>
          <p><span className={styles.label}>Улица</span>{user.address.street}</p>
        </React.Fragment>}
        {user.company !== undefined &&
        <p><span className={styles.label}>Компания</span>{user.company.name}</p>}
      </div>
    }
    return <div>
      Не удалось загрузить пользователя
    </div>
  }

}
