import {createSelector} from 'reselect'

const selectUser = (state) => state.User

const makeUser = createSelector(
  selectUser,
  (state) => state.get('user')
)

const makeLoad = createSelector(
  selectUser,
  (state) => state.get('load')
)

export {
  selectUser,
  makeLoad,
  makeUser
}
