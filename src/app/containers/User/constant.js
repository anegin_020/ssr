import defineConstant from '../../containers/helpers/defineConstant'

export const data = defineConstant('User', 'data', ['SUCCESS', 'REQUEST', 'FAILURE'])
export const destroy = defineConstant('User', 'destroy')
