import * as UserRecords from './record'

export const loadUserService = (api, id) => (
  api.get(`https://jsonplaceholder.typicode.com/users/${id}`)
    .then(response => ({response:
      UserRecords.UserRecord(response)
    }))
    .catch(errors => ({errors}))
)
