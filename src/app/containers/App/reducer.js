import Immutable from 'immutable'
import * as AppRecords from './record'
import * as AppConstants from './constant'

export const initialState = Immutable.Map({
  breadcrumbs: Immutable.List(),
  domain: 'http://aaaaa.netgamer.loc',
  counter: 1,
  photos: Immutable.List(),
  load: false,
  user: new AppRecords.UserRecord()
})

export default (state = initialState, action) => {
  switch (action.type) {
    case AppConstants.App_counter_GET:
      return state.get('counter')
    case AppConstants.App_counter_SET:
      return state.set('counter', action.payload)
    case AppConstants.App_photos:
      return state.set('photos', Immutable.List(action.payload))
        .set('load', true)
    case AppConstants.setBreadcrumbs:
      return state.set('breadcrumbs', Immutable.List(action.payload.map(
        breadcrumb => new AppRecords.BreadcrumbRecord(breadcrumb)
      )))
    default:
      return state
  }
}
