import defineConstant from '../helpers/defineConstant'

export const App_counter_GET = 'App_counter_GET'
export const App_counter_SET = 'App_counter_SET'
export const App_counter_ASYNC_SET = 'App_counter_ASYNC_SET'

export const App_photos = 'App_photos'

export const App_init = 'App_init'

export const setBreadcrumbs = `App_setBreadcrumbs`

