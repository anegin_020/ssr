import React from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import * as AppSelectors from './selector'
import * as AppActions from './action'
import Breadcrumbs from '../../components/Breadcrumbs'
import ContainerInterface from '../helpers/ContainerInterface'
const styles = require(`./styles/styles.${process.env.CURRENT_THEME}.scss`)

@connect((state) => createStructuredSelector({
  counter: AppSelectors.makeSelectCounter,
  photos: AppSelectors.makePhotos,
  load: AppSelectors.makeLoad,
  domain: AppSelectors.makeDomain,
  breadcrumbs: AppSelectors.makeBreadcrumbs,
  location: AppSelectors.makeLocation,
}))
export default class App extends ContainerInterface {

  static loadData = () => (AppActions.init())
  static checkLoadData = () => (AppSelectors.makeLoad)

  handleEvent(e) {
    switch (e.type) {
      case 'keydown':
        this.handleClickUp(e);
        break;
    }
  }

  handleClose() {
    console.log('close')
  }

  handleClickUp = (e) => {
    let modals = document.getElementsByClassName('Modal');
    if (modals[modals.length - 1] === this.refs['Modal']) {
      this.handleClose();
    }
    if (e.keyCode === 38)
      this.handleAddCounter()
  }

  componentDidMount() {
    if(this.props.load === false){
      this.props.dispatch(App.loadData())
    }
    document.addEventListener('keydown', this, false);
  }

  handleAddCounter = () => this.props.dispatch(AppActions.setAsyncCounter(this.props.counter + 1))

  render() {
    const {counter, photos, domain, children, load, breadcrumbs} = this.props
    console.log('RENDER APP')
    return (
      <div className={styles.body}>
        <h1 className={styles.h2}>Count sections: {counter} !!</h1>
        <p>domain: {domain}</p>
        <button onClick={this.handleAddCounter}>
          Add
        </button>
        <Breadcrumbs data={breadcrumbs}/>
        <h2>Photos({photos.count()}):</h2>
        <br/>
        <br/>
        {load === true && children}
      </div>
    )
  }

}
