import * as AppConstants from './constant'

export const setCounter = (data) => ({
  type: AppConstants.App_counter_SET,
  payload: data,
})

export const setAsyncCounter = (data) => ({
  type: AppConstants.App_counter_ASYNC_SET,
  payload: data,
})

export const init = () => ({
  type: AppConstants.App_init,
})

export const photosSuccess = (payload) => ({
  type: AppConstants.App_photos,
  payload,
})

export const setBreadcrumbs = (payload) => ({
  type: AppConstants.setBreadcrumbs,
  payload,
})
