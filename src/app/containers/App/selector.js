import { createSelector } from 'reselect'

const selectApp = (state) => state.App
const selectRouting = (state) => state.router

const makeSelectCounter = createSelector(
  selectApp,
  (state) => state.get('counter')
)

const makePhotos = createSelector(
  selectApp,
  (state) => state.get('photos')
)

const makeLoad = createSelector(
  selectApp,
  (state) => state.get('load')
)

const makeDomain = createSelector(
  selectApp,
  (state) => state.get('domain')
)

const makeBreadcrumbs = createSelector(
  selectApp,
  (state) => state.get('breadcrumbs')
)

const makeLocation = createSelector(
  selectRouting,
  (state) => state.location
)

const makeUser = createSelector(
  selectApp,
  (state) => state.get('user')
)

export {
  selectApp,
  makeSelectCounter,
  makePhotos,
  makeLoad,
  makeDomain,
  makeBreadcrumbs,
  makeLocation,
  makeUser,
}
