import {delay} from 'redux-saga'
import {all, takeEvery, takeLatest, put, call, select, fork, cancel} from 'redux-saga/effects'
import * as AppConstants from './constant'
import {getPhotosService} from './service'
import apiWorker from '../../utils/Api/ApiWorker'

function* asyncSetCounter({payload}) {
  yield delay(1000)
  yield put({type: AppConstants.App_counter_SET, payload})
}

function* init() {
  const {response, errors} = yield apiWorker(getPhotosService, 'var_1')

  if(response){
    yield put({
      type: AppConstants.App_photos,
      payload: response
    })
  }
}

export default function*() {
  yield all([
    yield takeEvery(AppConstants.App_counter_ASYNC_SET, asyncSetCounter),
    yield takeEvery(AppConstants.App_init, init),
  ])
}
