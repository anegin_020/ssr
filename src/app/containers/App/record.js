import Immutable from 'immutable'

export const BreadcrumbRecord = Immutable.Record({
  text: undefined,
  link: undefined,
})

export const UserRecord = Immutable.Record({
  auth: false,
  token: '',
  username: undefined,
  permissions: Immutable.List(['articles']),
})
