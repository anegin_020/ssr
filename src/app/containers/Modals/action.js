import * as ModalConstants from './constant'

/**
 * Add component to modal list
 * @param data
 * id string - required name of modal
 * content node - children element
 */
export const set = (data) => ({
  type: ModalConstants.Modal_set,
  payload: data
})

export const removeById = (id) => ({
  type: ModalConstants.Modal_remove_by_id,
  payload: id,
})

export const removeByUid = (uid) => ({
  type: ModalConstants.Modal_remove_by_uid,
  payload: uid,
})

export const destroy = () => ({
  type: ModalConstants.Modal_destroy,
})
