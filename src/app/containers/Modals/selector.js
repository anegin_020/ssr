import { createSelector } from 'reselect'

const selectModal = (state) => state.Modal

const makeModals = createSelector(
  selectModal,
  (state) => state.get('modals')
)

export {
  selectModal,
  makeModals,
}
