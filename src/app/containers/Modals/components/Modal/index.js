import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.scss'

export default class Modal extends React.PureComponent{

  handleClose = () => {
    this.props.onClose(this.props.uid)
  }

  componentWillUnmount(){
    if(this.props.onCloseCb !== undefined){
      this.props.onCloseCb()
    }
  }

  render(){
    const {content} = this.props
    return <section className={styles.body} onClick={this.handleClose}>
      <div className={styles.content}>
        {content}
      </div>
    </section>
  }

}

Modal.PropTypes = {
  onClose: PropTypes.func,
  onCloseCb: PropTypes.func,
  uid: PropTypes.number,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  content: PropTypes.node,
}
