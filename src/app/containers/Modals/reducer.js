import Immutable from 'immutable'
import * as ModalConstants from './constant'

let counterUid = 0

const initialState = Immutable.Map({
  modals: Immutable.List(),
})

export default function (state = initialState, action) {
  switch (action.type){
    case ModalConstants.Modal_set:
      return state.set('modals', state.get('modals').add({
        ...action.payload,
        uid: counterUid++,
      }))
    case ModalConstants.Modal_remove_by_uid:
      return state.set('modals', state.get('modals').filter(modal =>
        modal.uid !== action.payload
      ))
    case ModalConstants.Modal_remove_by_id:
      return state.set('modals', state.get('modals').filter(modal =>
        modal.id !== action.payload
      ))
    case ModalConstants.Modal_destroy:
      return initialState
    default:
      return state
  }
}
