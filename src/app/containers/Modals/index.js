import React from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import * as ModalSelector from './selector'
import Modal from './components/Modal'
import PropTypes from 'prop-types'

@connect((state) => createStructuredSelector({
  modals: ModalSelector.makeModals,
}))
export default class Modals extends React.Component{

  render(){
    const {modals} = this.props
    return <React.Fragment>
      {modals.map((modal, key) => <Modal key={key} {...modal} />)}
    </React.Fragment>
  }

}

Modals.PropTypes = {
  modals: PropTypes.any,
}
