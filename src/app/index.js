import React from 'react'
import ReactDOM from 'react-dom'
import {createBrowserHistory} from 'history'
import Router from './Router'
import Store from './store'
import Root from './root/clientRoot'

const MOUNT_NODE = document.getElementById('app')
const history = createBrowserHistory()
const store = Store(history, window.__INITIAL_STATE__)

delete window.__INITIAL_STATE__;

// history.listen((location, action) => {
//   console.log("HISTORY.LISTEN", location, action)
// });

ReactDOM.render(
  <Root
    store={store}
    routes={<Router/>}
    history={history}
  />,
  MOUNT_NODE
);

