import { createStore, applyMiddleware, compose } from 'redux';
import {createLogger} from 'redux-logger';
import createSagaMiddleware, { END } from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import {deserialize} from '../utils/SerializeRule'
import rootReducer from '../rootReducer';
import rootSaga from '../rootSaga';

export default function configureStore(history, initialState) {
  const sagaMiddleware = createSagaMiddleware();
  const browserHistoryMiddleware = routerMiddleware(history);
  const store = createStore(
    rootReducer,
    deserialize(initialState),
    compose(
      applyMiddleware(
        browserHistoryMiddleware,
        sagaMiddleware,
        createLogger()
      ),
    )
  );

  let saga = store.runSaga = sagaMiddleware.run(rootSaga);
  store.close = () => store.dispatch(END);

  if (module.hot) {
    module.hot.accept('../rootReducer', () => {
      const nextRootReducer = require('../rootReducer').default;
      store.replaceReducer(nextRootReducer)
    })
    module.hot.accept('../rootSaga', () => {
      const nextSaga = require('../rootSaga').default;
      saga.cancel()
      saga.done.then(() => {
        saga = sagaMiddleware.run(function* replacedSaga (action) {
          yield nextSaga()
        })
      })
    })
  }

  return store
}
