import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import {deserialize} from '../utils/SerializeRule'
import rootReducer from '../rootReducer';
import rootSaga from '../rootSaga';

export default function configureStore(history, initialState) {
  const sagaMiddleware = createSagaMiddleware();
  const browserHistoryMiddleware = routerMiddleware(history);
  const store = createStore(
    rootReducer,
    deserialize(initialState),
    applyMiddleware(
      browserHistoryMiddleware,
      sagaMiddleware,
    )
  );

  store.runSaga = sagaMiddleware.run(rootSaga);
  console.log('GET_SATE', store.getState())
  store.close = () => store.dispatch(END);
  return store
}
