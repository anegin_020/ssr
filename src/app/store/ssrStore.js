import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware, { END } from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import rootReducer from '../rootReducer';

export default function configureStore(history, initialState) {
  const sagaMiddleware = createSagaMiddleware();
  const browserHistoryMiddleware = routerMiddleware(history);
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      browserHistoryMiddleware,
      sagaMiddleware,
    )
  );

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);
  return store
}
