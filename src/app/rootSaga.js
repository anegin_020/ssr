import {all} from 'redux-saga/effects'
import AppSagas from './containers/App/saga'
import UsersSagas from './containers/Users/saga'
import UserSagas from './containers/User/saga'
import HomeSaga from './containers/Home/saga'

export default function *() {
  let rootSagas = [
    AppSagas(),
    UsersSagas(),
    UserSagas(),
    HomeSaga(),
  ]

  if(process.env.NODE_APP === 'server') rootSagas.push((require('./Router/saga').default)())

  yield all(rootSagas)
}
