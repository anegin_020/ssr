import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'

export default ({store, routes, history, context = {}}) => (<Provider store={store}>
  <ConnectedRouter history={history} context={context} isSSR={true}>
    {routes}
  </ConnectedRouter>
</Provider>)
