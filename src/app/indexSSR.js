import Router from './Router'
import createStore from './store/ssrStore'
import Root from './root/ssrRoot'
import rootSaga from './rootSaga'
import * as SerializeRule from './utils/SerializeRule'

export default {
  Router,
  createStore,
  Root,
  rootSaga,
  SerializeRule,
}
