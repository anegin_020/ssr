import '../utils/IsomorphicFetch'
import Express from 'express'
import React from 'react'
import bodyParser from 'body-parser'
import Logger from '../utils/Logger'
import AppReducer from '../../src/app/containers/App/reducer'
import {initialState} from '../../src/app/containers/App/reducer'
import {renderToString} from 'react-dom/server'
import {createMemoryHistory} from 'history'
import {replace} from 'react-router-redux'

const argv = require('minimist')(process.argv.slice(2))


global.document = {};
global.site = '';

const app = Express()
const manifest = require(`../../public/${process.env.CURRENT_THEME}/manifest.json`)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use('/assets', Express.static(`public/${process.env.CURRENT_THEME}/assets`))

// This is fired every time the server side receives a request
app.get('*', handleRender)

// We are going to fill these out in the sections to follow
function handleRender(req, res) {

  const SSR = process.env.NODE_ENV === 'development'
    ? require(`../../src/app/indexSSR`).default
    : require(`../../dist/${process.env.CURRENT_THEME}/bundle`).default
  const appReducer = {App: AppReducer(initialState.set('domain', 'http://aaaaa.netgamer.loc'), {})}


  const memoryHistory = createMemoryHistory({initialEntries: [req.url]})
  const store = SSR.createStore(memoryHistory, appReducer)

  const root = <SSR.Root
    history={memoryHistory}
    store={store}
    routes={SSR.Router(store)}
  />
  memoryHistory.listen((state)=>console.log)
  const runningSaga = store.runSaga(SSR.rootSaga)
  runningSaga.done.then(() => {
    const markup = renderToString(root);
    console.log('DONE'/*, memoryHistory*/);
    if(req.url !== store.getState().router.location.pathname){
      runningSaga.cancel()
      store.close();
      res.redirect(store.getState().router.location.pathname)
    } else {
      //console.log('then', store.getState().router)
      const html = renderFullPage(markup, store, SSR.SerializeRule.serialize);
      //console.log('TEST', store.getState().router)
      res.status(200).send(html)
    }
  })

  renderToString(root);
  store.close();

}

function renderFullPage(html, preloadedState, serialize) {
  return `
    <!doctype html>
    <html>
      <head>
        <title>Development</title>
        <link rel="shortcut icon" href="/assets/files/favicon.png" type="image/png">
        <link rel="stylesheet" href="/assets/style/${manifest['main.css']}"/>
      </head>
      <body>
        <div id="app">${html}</div>
        <div id="appModals"></div>
        <script>
          window.__INITIAL_STATE__=${serialize(preloadedState ? preloadedState.getState() : {})};
        </script>
        <script src="/assets/js/${manifest['main.js']}"></script>
      </body>
    </html>
    `
}

function handleRenderPost(req, res) {
  /*const store = createStore()
   const root = <Root
   location={req.originalUrl}
   store={store}
   routes={Router(store)}
   />

   store.runSaga(rootSaga).done.then(()=>{
   const markup = renderToString(root);
   const html = renderFullPage(markup, store);
   res.status(200).send(html)
   })

   renderToString(root);
   store.close();*/
  site = req.hostname
  console.log('Headers', Headers());
  fetch('/api/articles')
    .then(response => response.json())
    .then(response => {
      console.log(req.hostname)
      res.json({response});
    })
    .catch(errors => {
      console.log({errors})
      res.json({site: req.hostname})
    })
}

function renderContent() {

}

app.post('*', handleRenderPost)
const port = parseInt(argv.port || process.env.PORT || '1962', 10);
const host = argv.host || process.env.HOST || 'localhost';

app.listen(port, host, (err) => {
  if (err) {
    return Logger.error(err.message)
  }

  Logger.info(`Start on localhost:${port}`)
});
