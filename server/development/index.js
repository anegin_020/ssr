require('dotenv').config()
const Logger = require('../../internals/scripts/Logger')
const express = require('express')
const setup = require('./middlewares/frontendMiddleware')
const resolve = require('path').resolve
const argv = require('minimist')(process.argv.slice(2))
const app = express()

setup(app, {
    outputPath: resolve(process.cwd(), 'public', process.env.CURRENT_THEME),
    publicPath: '/',
});

const port = parseInt(argv.port || process.env.PORT || '3000', 10);
const host = argv.host || process.env.HOST || 'localhost';

app.listen(port, host, (err) => {
    if (err) {
        return Logger.error(err.message)
    }

    Logger.info(`Start on ${host}:${port} with '${process.env.NODE_ENV}' mode`)
});
