const chalk = require("chalk")

module.exports = {
  error: (data) => console.error(chalk.red(data)),
  info: (data) => console.log(chalk.blue(data)),
  warning: () => console.warn(chalk.yellow(data)),
}
