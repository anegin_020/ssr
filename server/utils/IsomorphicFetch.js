var realFetch = require('node-fetch');
module.exports = function(url, options) {
  if (/^\/\//.test(url)) {
    url = 'https:' + url;
  }
  if(typeof url === 'string' && url.match(/^https?/) !== null){
    return realFetch.call(this, url, options);
  } else {
    return realFetch.call(this, `http://${site}${url}`, options);
  }

};

if (!global.fetch) {
  global.fetch = module.exports;
  global.Response = realFetch.Response;
  global.Headers = realFetch.Headers;
  global.Request = realFetch.Request;
}
